/**
 * Copyright (c) 2014 Denis Meyer
 */
package org.opencastproject.engage.theodul.plugin.custom.snowShowcase;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.opencastproject.engage.theodul.api.AbstractEngagePlugin;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/")
public class EngagePluginCustomSnowShowcase extends AbstractEngagePlugin {

  private static final Logger log = LoggerFactory.getLogger(EngagePluginCustomSnowShowcase.class);
  
  protected void activate(ComponentContext cc) {
    log.info("Activated.");
  }
  /*
  @GET
  @Path("sayhello")
  @Produces(MediaType.TEXT_PLAIN)
  public String sayHello() {
    return "Bilbos Schatz!";
  }*/
}
