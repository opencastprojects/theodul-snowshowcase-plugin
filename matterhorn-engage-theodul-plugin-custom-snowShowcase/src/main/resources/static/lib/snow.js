/*
 * Snowflake jQuery plugin
 * 
 * Copyright (c) 2014 Denis Meyer
 * Based on the goat1000 snow script
 */
(function($) {
    var settings;
    var ct;
    var cv = null;
    var flakes = [];
    var ct;
    var gt;
    var imageLoaded = false;
    var imageObj = null;
    $.snow = function(options) {
        settings = $.extend({
            count: 60,
            delay: 20,
            flutter: 0.2,
            wobble: 0.5,
            spin: 1.0,
            wind: 1.0,
            w1: 1,
            minSpeed: 0.3,
            maxSpeed: 4,
            autostart: true,
            repeat: "repeat",
            bgimg: "",
            bgadd: false
        }, options);
        if(settings.autostart) {
            start();
        }
    }

    function toggle() {
        if (window.snowtimer) {
            stop();
        } else {
            start();
        }
    }

    function resize() {
        cv.width = innerWidth;
        cv.height = innerHeight;
        if(settings.bgadd) {
            imageObj = new Image();
            imageObj.onload = function() {
                imageLoaded = true;
            };
            imageObj.src = settings.bgimg;
        }
    }

    function start() {
        if (!window.snowtimer) {
            cv = document.createElement('canvas');
            cv.width = cv.height = 10; // set initial size
            cv.id = 'backgroundSnowCanvas';
            document.body.appendChild(cv);
            createFlake();
            ct = cv.getContext('2d');
            cv.style.position = 'fixed';
            cv.style.top = 0;
            cv.style.left = 0;
            cv.style.zIndex = -1;
            resize();
            var c = settings.count;
            flakes = [];
            do {
                flakes.push(new flake());
            } while (--c);
            ct.fillRect(0, 0, cv.width, cv.height);
            window.snowtimer = window.setInterval(draw, settings.delay);
            window.addEventListener('resize', resize);
        }
    }

    function stop() {
        if (window.snowtimer) {
            window.clearInterval(window.snowtimer);
            var c = document.getElementById('backgroundSnowCanvas');
            c.parentNode.removeChild(c);
            window.snowtimer = snow = null;
        }
    }

    function draw() {
        var f = flakes;
        var c = settings.count;
        ct.fillRect(0, 0, cv.width, cv.height);
        if(imageLoaded && settings.bgadd) {
            ct.drawImage(imageObj, 0, 0, imageObj.width, imageObj.height, 0, 0, cv.width, cv.height);
        }

        do {
            f[--c].draw(ct) && ++fdone;
        } while (c);
        settings.wind += Math.cos(settings.w1++/ 180.0);
    }

    function flake() {
        this.draw = function(ct) {
            var x = this.x + settings.wind,
                y = this.y,
                cx = x + this.sz / 2,
                cy = y + this.sz / 2;
            ct.translate(cx, cy);
            ct.rotate(this.a);
            ct.translate(-cx, -cy);
            ct.drawImage(flakeImages[this.flake], x, y, this.sz, this.sz);
            if (this.flakebits >= 0)
                ct.drawImage(flakeBits[this.flakebits], x, y, this.sz, this.sz);
            ct.setTransform(1, 0, 0, 1, 0, 0);
            this.animate();
        };
        this.animate = function() {
            this.y += this.speed;
            this.x += this.flutter * Math.cos(settings.flutter * settings.flutter * this.y);
            this.a = (this.spin * this.y) + (this.wobble * Math.sin(this.y / this.sz));
            if (this.y > innerHeight)
                this.init(1);
        };
        this.init = function(f) {
            this.speed = settings.minSpeed + (Math.random() * (settings.maxSpeed - settings.minSpeed));
            this.sz = ~~(Math.random() * 40) + 20;
            this.flutter = ~~(Math.random() * settings.flutter * (60 - this.sz));
            this.wobble = Math.random() * settings.wobble;
            this.spin = settings.spin * 0.1 * (Math.random() - 0.5);
            this.a = 0;
            this.x = (Math.random() * (innerWidth + this.sz)) - this.sz;
            this.y = f ? -this.sz : Math.random() * innerHeight;
            this.flake = ~~(Math.random() * flakeImages.length);
            this.flakebits = ~~(Math.random() * (flakeBits.length + 1)) - 1;
        };
        this.init();
    }

    function createFlake() {
        var f, g, c, fi, w, bitfunc, bfns;
        var cv = document.createElement('canvas');
        cv.width = cv.height = 40;
        flakeImages = [];
        flakeBits = [];

        for (f = 0; f < 6; ++f) {
            flakeImages[f] = fi = cv.cloneNode();
            c = fi.getContext('2d');
            c.fillStyle = '#fff';
            c.translate(20, 20);
            c.beginPath();
            w = 1 + (f / 2);
            c.rect(-w, -20, w * 2, 40);
            c.rotate(Math.PI / 3.0);
            c.rect(-w, -20, w * 2, 40);
            c.rotate(Math.PI / 3.0);
            c.rect(-w, -20, w * 2, 40);
            c.closePath();
            c.fill();
        }

        function ball(c) {
            c.arc(0, -16, 4, 0, 7, 0);
        }

        function straightbit(c, w, x, y, z, a) {
            c.moveTo(0, -x);
            c.lineTo(w, -y);
            c.lineTo(w, -z);
            c.lineTo(0, -y - a);
            c.lineTo(-w, -z);
            c.lineTo(-w, -y);
            c.lineTo(0, -x);
        }

        function arm(c) {
            straightbit(c, 5, 6, 8, 10, 0);
        }

        function arm2(c) {
            straightbit(c, 5, 10, 12, 14, 0);
        }

        function lump(c) {
            straightbit(c, 4, 6, 8, 12, 2);
        }

        function fluff(c) {
            straightbit(c, 9, 6, 8, 11, 0);
        }

        function bar(c) {
            straightbit(c, 4, 16, 16, 18, 2);
        }

        function bar2(c) {
            straightbit(c, 5, 12, 12, 14, 2);
        }

        function balllump(c) {
            ball(c);
            lump(c);
        }

        function armlump(c) {
            arm2(c);
            lump(c);
        }

        function twoarm(c) {
            arm(c);
            arm2(c);
        }

        function ballarm(c) {
            ball(c);
            arm(c);
        }

        function twobar(c) {
            bar(c);
            bar2(c);
        }

        function barfluff(c) {
            bar(c);
            fluff(c);
        }

        function barlump(c) {
            bar(c);
            lump(c);
        }

        function bararm(c) {
            bar(c);
            arm(c);
        }

        function bar2arm(c) {
            bar2(c);
            arm(c);
        }

        function bararm2(c) {
            bar(c);
            arm2(c);
        }

        function ballfluff(c) {
            ball(c);
            fluff(c);
        }
        bfns = [ballfluff, bar2arm, bararm2, bararm, barlump, barfluff, bar, bar2,
            twobar, fluff, ball, arm, arm2, lump, balllump, armlump, twoarm, ballarm
        ];

        for (f = 0; f < bfns.length; ++f) {
            flakeBits[f] = fi = cv.cloneNode();
            bitfunc = bfns[f];
            c = fi.getContext('2d');
            c.translate(20, 20);
            c.fillStyle = '#fff';
            for (g = 0; g < 6; ++g) {
                c.beginPath();
                bitfunc(c);
                c.closePath();
                c.fill();
                c.rotate(Math.PI / 3.0);
            }
        }
    }
    
    $(document).on("snow:start", function(e) {
        start();
    });
    $(document).on("snow:stop", function(e) {
        stop();
    });
    $(document).on("snow:addbg", function(e) {
        settings.bgadd = true;
        resize();
    });
    $(document).on("snow:removebg", function(e) {
        settings.bgadd = false;
        resize();
    });
})(jQuery);
