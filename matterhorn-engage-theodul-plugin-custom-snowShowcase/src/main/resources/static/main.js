/**
 * Copyright (c) 2014 Denis Meyer
 */
/*jslint browser: true, nomen: true*/
/*global define*/
define(["require", "jquery", "underscore", "backbone", "engage/core", "moment"], function(require, $, _, Backbone, Engage, Moment) {
    "use strict";
    var PLUGIN_NAME = "Engage Custom SnowShowcase";
    var PLUGIN_TYPE = "engage_custom";
    var PLUGIN_VERSION = "1.0";
    var PLUGIN_TEMPLATE = "none";
    var PLUGIN_TEMPLATE_MOBILE = "none";
    var PLUGIN_TEMPLATE_EMBED = "none";
    var PLUGIN_STYLES = [];
    var PLUGIN_STYLES_MOBILE = [];
    var PLUGIN_STYLES_EMBED = [];

    var plugin;
    var events = {
        plugin_load_done: new Engage.Event("Core:plugin_load_done", "when the core loaded the event successfully", "handler")
    };

    var isDesktopMode = false;
    var isEmbedMode = false;
    var isMobileMode = false;

    // desktop, embed and mobile logic
    switch (Engage.model.get("mode")) {
        case "mobile":
            plugin = {
                name: PLUGIN_NAME,
                type: PLUGIN_TYPE,
                version: PLUGIN_VERSION,
                styles: PLUGIN_STYLES_MOBILE,
                template: PLUGIN_TEMPLATE_MOBILE,
                events: events
            };
            isMobileMode = true;
            break;
        case "embed":
            plugin = {
                name: PLUGIN_NAME,
                type: PLUGIN_TYPE,
                version: PLUGIN_VERSION,
                styles: PLUGIN_STYLES_EMBED,
                template: PLUGIN_TEMPLATE_EMBED,
                events: events
            };
            isEmbedMode = true;
            break;
        case "desktop":
        default:
            plugin = {
                name: PLUGIN_NAME,
                type: PLUGIN_TYPE,
                version: PLUGIN_VERSION,
                styles: PLUGIN_STYLES,
                template: PLUGIN_TEMPLATE,
                events: events
            };
            isDesktopMode = true;
            break;
    }

    /* change these variables */
    var snowPath = "lib/snow";

    /* don't change these variables */
    var initCount = 2;

    /**
     * Initialize the plugin
     */
    function initPlugin() {
        try {
            window.setTimeout(function() {
                // jquery calls just for demo purposes, don't do it that way in a "real" plugin!
                $("#engage_controls_second, #engage_tab_content").css("background-color", "transparent");
                $("#engage_basic_description, #engage_description_tab_content, #engage_tab_downloadableVideos_content, #engage_shortcuts_tab_content, #engage_slidetext_tab_content").css("color", "#FFFFFF");
                $(".table").removeClass("table-striped");
            }, 1000);
            $.snow({
                count: 30,
                delay: 20,
                minSpeed: 2,
                maxSpeed: 5,
                autostart: true
            });
        } catch (e) {
            // console.log(e);
            console.log("No snow for you...");
        }
    }

    // init event
    Engage.log("SnowShowcase: Init");
    var relative_plugin_path = Engage.getPluginPath("EngagePluginCustomSnowShowcase");

    // all plugins loaded
    Engage.on(plugin.events.plugin_load_done.getName(), function() {
        Engage.log("SnowShowcase: Plugin load done");
        initCount -= 1;
        if (initCount <= 0) {
            initPlugin();
        }
    });

    // load snow lib
    require([relative_plugin_path + snowPath], function() {
        Engage.log("SnowShowcase: Lib snow loaded");
        initCount -= 1;
        if (initCount <= 0) {
            initPlugin();
        }
    });

    return plugin;
});
