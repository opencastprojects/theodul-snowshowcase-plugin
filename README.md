Theodul Pass Player SnowShowcase Plugin
=====

A plugin for the new Matterhorn Engage Theodul Player.
Let it snow!

Copyright 2015 Denis Meyer, License: GNU LGPL v2 or v3

![Screenshot](img/screenshot.png "Screenshot")

Installing
-----
To add the plugin to your existing theodul installation, just download the compiled plugin jar file (under "Downloads") and copy it into your Matterhorn lib folder.

Compiling
-----
As usual: mvn clean install -DdeployTo=/your/path/to/matterhorn
